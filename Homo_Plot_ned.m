function array = Homo_Plot(theta_array)
%% DH파라미터 기입
d1 = 171.5;
d2 = 0;
d3 = 0;
d4 = 245;
d5 = 0;
d6 = 43;

al1 = -90;
al2 = 0;
al3 = -90;
al4 = 90;
al5 = -90;
al6 = 0;

a1 = 0;
a2 = 221;
a3 = 32.5;
a4 = 0;
a5 = 9;
a6 = 0;

q1 = theta_array(1);
q2 = theta_array(2);
q3 = theta_array(3);
q4 = theta_array(4);
q5 = theta_array(5);
q6 = theta_array(6);
%% 동차변환
T01 = DHmatrix(q1,d1,a1,al1);
T12 = DHmatrix(q2,d2,a2,al2);
T23 = DHmatrix(q3,d3,a3,al3);
T34 = DHmatrix(q4,d4,a4,al4);
T45 = DHmatrix(q5,d5,a5,al5);
T56 = DHmatrix(q6,d6,a6,al6);

T02 = T01 * T12;
T03 = T02 * T23;
T04 = T03 * T34;
T05 = T04 * T45;
T06 = T05 * T56;

P0 = [0;0;0];
P1 = T01(1:3,4);
P2 = T02(1:3,4);
P3 = T03(1:3,4);
P4 = T04(1:3,4);
P5 = T05(1:3,4);
P6 = T06(1:3,4);
%% plot array

Px = [0 P1(1) P2(1) P3(1) P4(1) P5(1) P6(1)];
Py = [0 P1(2) P2(2) P3(2) P4(2) P5(2) P6(2)];
Pz = [0 P1(3) P2(3) P3(3) P4(3) P5(3) P6(3)];

array = [Px;Py;Pz];
end