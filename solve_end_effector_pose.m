function P6 = solve_end_effector_pose(theta_array)

%% DH파라미터 기입
d1 = 183;
d2 = 0;
d3 = 0;
d4 = 221.5;
d5 = 0;
d6 = 112.7;

al1 = -90;
al2 = 0;
al3 = -90;
al4 = 90;
al5 = -90;
al6 = 0;

a1 = 0;
a2 = 210;
a3 = 30;
a4 = 0;
a5 = -5.5;
a6 = 0;
%% 각 관절의 각도 받아오기
theta_array = theta_array;

q1 = theta_array(1);
q2 = theta_array(2);
q3 = theta_array(3);
q4 = theta_array(4);
q5 = theta_array(5);
q6 = theta_array(6);

%% 동차변환
T01 = DHmatrix(q1,d1,a1,al1);
T12 = DHmatrix(q2,d2,a2,al2);
T23 = DHmatrix(q3,d3,a3,al3);
T34 = DHmatrix(q4,d4,a4,al4);
T45 = DHmatrix(q5,d5,a5,al5);
T56 = DHmatrix(q6,d6,a6,al6);
   
%___________________________________________________________________________________________%

T02 = T01 * T12;
T03 = T02 * T23;
T04 = T03 * T34;
T05 = T04 * T45;
T06 = T05 * T56;

% P vector
P6 = [T06(1:3,4);0;0;0];
end