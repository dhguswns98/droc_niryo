%% Niryo One 시뮬레이션 코드
%% 2022/08/02 리빌딩
%% Version 0.0.2
% 수정사항
% 1. 코드 간결화
% 2. quentic 적용
% 3. error보정 없이 trajectory 설정하기
%% Goal
% 홈 포지션에서 목표 지점으로 엔드이펙터를 이동시킨다.
% 두 개의 좌표만 사용하여 시물레이션 성공하기.
%% 메모
% 사실상 오차는 거의 없음.
% Symbolic Math Toolbox 이용하여 오차를 아예 줄이는 방법으로 차후 목표 잡기.
%% 
clear
clc
format compact
%% 다항식 궤적 5th order polynomial trajectory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 데이터 초기화
vi = [0;0;0;0;0;0]; vf = [0;0;0;0;0;0]; ai = [0;0;0;0;0;0]; af = [0;0;0;0;0;0];
ti = 0; tf = 5;
t = linspace(ti,tf,100*(tf-ti));

Point_Home = [334 0 417 0 0 0];
TargetPoint_1 = [125 150 350 0 0 0];
Joint_Home = [0;-pi/2;0;0;0;0];
%% 
M = [1 ti ti^2 ti^3 ti^4 ti^5;
     0 1 2*ti 3*(ti^2) 4*(ti^3) 5*(ti^4);
     0 0 2 6*ti 12*(ti^2) 20*(ti^3);
     1 tf tf^2 tf^3 tf^4 tf^5;
     0 1 2*tf 3*(tf^2) 4*(tf^3) 5*(tf^4);
     0 0 2 6*tf 12*(tf^2) 20*(tf^3)];

b = [Point_Home' vi ai TargetPoint_1' vf af]';

a = M\b;

t = linspace(ti,tf,100*(tf-ti));

c = ones(size(t));
%%
pxd = a(1).*c + a(2).*t +a(3).*t.^2 + a(4).*t.^3 +a(5).*t.^4 + a(6).*t.^5;
vxd = a(2).*c +2*a(3).*t +3*a(4).*t.^2 +4*a(5).*t.^3 +5*a(6).*t.^4;
axd = 2*a(3).*c + 6*a(4).*t +12*a(5).*t.^2 +20*a(6).*t.^3;

pyd = a(1,2).*c + a(2,2).*t +a(3,2).*t.^2 + a(4,2).*t.^3 +a(5,2).*t.^4 + a(6,2).*t.^5;
vyd = a(2,2).*c +2*a(3,2).*t +3*a(4,2).*t.^2 +4*a(5,2).*t.^3 +5*a(6,2).*t.^4;
ayd = 2*a(3,2).*c + 6*a(4,2).*t +12*a(5,2).*t.^2 +20*a(6,2).*t.^3;

pzd = a(1,3).*c + a(2,3).*t +a(3,3).*t.^2 + a(4,3).*t.^3 +a(5,3).*t.^4 + a(6,3).*t.^5;
vzd = a(2,3).*c +2*a(3,3).*t +3*a(4,3).*t.^2 +4*a(5,3).*t.^3 +5*a(6,3).*t.^4;
azd = 2*a(3,3).*c + 6*a(4,3).*t +12*a(5,3).*t.^2 +20*a(6,3).*t.^3;
%% 샘플링 시간동안 반복

for k = 1:(length(t)-1)
    if k == 1
        q_dot_f = solve_jacobian(Joint_Home)\[vxd(k+1);vyd(k+1);vzd(k+1);0;0;0];
        qf = Joint_Home + q_dot_f*(t(k+1)-t(k));
        %
        Plot_Points = Homo_Plot(qf'.*(180/pi));
        plot3(Plot_Points(1,:),Plot_Points(2,:),Plot_Points(3,:),'-or','LineWidth',5);
        axis([-1000,1000,-1000,1000,0,1000]);
        grid on
    else
        vf = [vxd(k+1);vyd(k+1);vzd(k+1);0;0;0];
        q_dot_f = solve_jacobian(qf'.*(180/pi))\vf;
        qf = qf + q_dot_f*(t(k+1)-t(k));
        %
        Plot_Points = Homo_Plot(qf'.*(180/pi));
        plot3(Plot_Points(1,:),Plot_Points(2,:),Plot_Points(3,:),'-or','LineWidth',5);
        axis([-1000,1000,-1000,1000,0,1000]);
        grid on
        pause(0.0000001)
    end
end